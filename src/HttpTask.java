import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpTask {

    private static final String USER_AGENT = "Mozilla/5.0";

    public static final String GET_URL = "https://ya.ru/";

    public static final String POST_URL = "http://httpbin.org/post";

    public static void main(String[] args) throws IOException {

        sendGET();
        System.out.println("GET DONE");

        sendPOST();
        System.out.println("POST DONE");
    }

    public static void sendGET() throws IOException {

        URL obj = new URL(GET_URL);

        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-agent", USER_AGENT);
        int responseCode = connection.getResponseCode();

        System.out.println("GET response code: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK){

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
        }
        else{
            System.out.println("GET imposible");
        }
    }

    public static void sendPOST() throws IOException {

        URL obj = new URL(POST_URL);

        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-agent", USER_AGENT);
        int responseCode = connection.getResponseCode();

        System.out.println("POST Response Code : " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK){

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
        }
        else{
            System.out.println("POST imposible");
        }
    }
}
